import javax.swing.JFrame;

/*
 * 資管4A
 * 105403031  莫智堯
 *
 * Login password: 1234
 */

public class Main extends HW3_105403031 {

    public static void main(String args[]){
        HW3_105403031 HW3_drawer = new HW3_105403031();
        HW3_drawer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        HW3_drawer.setSize(700, 600);
        HW3_drawer.setVisible(true);
    }
}
